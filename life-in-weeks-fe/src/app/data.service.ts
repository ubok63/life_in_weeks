import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { User } from './app.component';
import jwtDecode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class DataService {


  baseUrl = 'http://localhost/api';
  constructor(private http: HttpClient) { }
  events: Event[];
  users: User[];
  userId: any[];
  decoded: any[];
  token: string;

  send(event: Event): Observable<Event[]>{
    const headers = new HttpHeaders({Authorization: 'Bearer ' + this.token});
    const body = {data: event};
    return this.http.post<Event>(`${this.baseUrl}/store`, body, {headers}).pipe(map((res) =>{
      console.log(res);
      this.events.push(res['data']);
      return this.events;
    }));
  }
 
  getAll(): Observable<Event[]>{
    /*const params = new HttpParams()
      .set('id', id.toString());*/
    const httpOptions = {/*params: params, */headers: new HttpHeaders({Authorization: 'Bearer ' + this.token})};

    return this.http.get(`${this.baseUrl}/list`, httpOptions).pipe(
      map((res) => {
        this.events = res['data'];
        return this.events;
      }),
      catchError(this.handleError));
  }

  login(login: string, pass: string): Observable<any[]>{
    const params = new HttpParams()
      .set('login', login).set('password', pass);

    return this.http.post<any>(`${this.baseUrl}/login`, {'login': login, 'password': pass}).pipe(
      map((res) => {
        this.token = res['data'];
        this.decoded = jwtDecode(res['data']);
        return this.decoded['data']['id'];
      }));
  } 

  delete(id: number): Observable<Event[]> {
    const params = new HttpParams()
      .set('id', id.toString());
    const httpOptions = {params:params, headers: new HttpHeaders({Authorization: 'Bearer ' + this.token})};

    return this.http.delete(`${this.baseUrl}/delete`, httpOptions)
      .pipe(map(res => {
        const filterEvents = this.events.filter((event) => {
          return +event['id'] !== +id;
        });
        return this.events = filterEvents;
      }),
      catchError(this.handleError));
}

logout(){
  this.token = "";
  this.decoded = [];
}

  private handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError('Error! something went wrong.');
  }

}
