import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../data.service';
//import { Event } from '../events/events.component';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
//import { Console } from 'console';

@Component({
  selector: 'app-weeks',
  templateUrl: './weeks.component.html',
  styleUrls: ['./weeks.component.css']
})
export class WeeksComponent implements OnInit {
  week: string = '';
  yearIn: string = '';
  mostCommon = '';
  mostImportant = '';
  user_id: number = 1;
  
  _yearsToShow: number = 0;
  fakeArray: number[] = [];
  currentYear: number = 2021
  weeksOfYear: number[] = new Array(52);
  event: Event;
  error = '';
  success = '';
  events: Event[] = [];
  mostCommonShow = '';
  mostImportantShow = '';
  _loggedUserId:number = 0;
  selectedEvent: Event;

  @Input('_loggedUser')
  set _loggedUser(lu:number){
    this._loggedUserId = lu;
  }
  eventt: any;
  constructor(private dataService: DataService){
  }
  @Input('year')
  set yearsToShow(yrs: number){
    this._yearsToShow = yrs;
  }
  

  ngOnInit(): void {
    if(this._yearsToShow < this.currentYear){
    this.fakeArray = new Array((this.currentYear - this._yearsToShow) + 1);
    for(let i = 0; i < this.fakeArray.length; i++){
      this.fakeArray[i] = this._yearsToShow++;
    }
  }else{
    this.fakeArray.length = 0;
  }
    for(let i = 0; i < this.weeksOfYear.length; i++){
      this.weeksOfYear[i] = i+1;
    }
    this.getEvents();
    this.eventt = {year: '', week: '', mostCommon: '', mostImportant: '', user_id: this._loggedUserId};
  }

  onSubmit(data: any){
    this.dataService.send(this.eventt).subscribe(
    (res: Event[]) =>{
      this.events = res;
      data.reset;
    }
    );
  }

  showEvent(x: number, y:number){
    let tmp = 0;
        for(let i = 0; i < this.events.length; i++){
          if(this.events[i]['year'] == x && this.events[i]['week'] == y){
            this.mostCommonShow = this.events[i]['mostCommon'];
            this.mostImportantShow = this.events[i]['mostImportant'];
            this.selectedEvent = this.events[i];
            tmp++;
          }
        }
        if(tmp == 0){
          this.mostCommonShow = '';
          this.mostImportantShow = '';
        }
        this.eventt = {year: x, week: y, mostCommon: '', mostImportant: '', user_id: this._loggedUserId};
  }

  getEvents(): void {
      this.dataService.getAll().subscribe(
      (res: Event[]) => {
        this.events = res;
        console.log(this.events);
      },
      (err) => {
        this.error = err;
      }
    );
  }

  isThereEvent(y: number, x: number): boolean{
    for(let i = 0; i < this.events.length; i++){
      if(this.events[i]['year'] == y && this.events[i]['week'] == x){
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    this.success = '';
    this.error   = '';
    
    this.dataService.delete(+id)
      .subscribe(
        (res: Event[]) => {
          this.events = res;
          this.success = 'Deleted successfully';
        },
        (err) => this.error = err
      );
}

deleteEvent(){
  this.delete(this.selectedEvent['id']);
  this.selectedEvent = null;
  this.mostImportantShow = '';
  this.mostCommonShow = '';
}

}

export class Event {
  

  constructor(
  week: string,
  year: string,
  mostCommon: string,
  mostImportant: string,
  user_id?: number,
  id?: number
  ) {
    
  }

  
}
