import { stringify } from '@angular/compiler/src/util';
import { Component } from '@angular/core';
import { UrlCreationOptions } from '@angular/router';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Life in weeks';
  loggedIn: boolean;
  user: any = {name: '', password: ''};
  ids: any[] = [];
  error= '';
  loggedUserId:number;
  badCreds: boolean = false;

  constructor(private dataService: DataService){
    
  }

  onSubmit(data:any){
    
    this.getUser(data.name, data.password);
    let check = 0;
      if(this.ids != undefined && this.ids.length != 0){
        this.loggedIn = true;
        this.loggedUserId = this.ids[0];
        this.badCreds = false;
        check++;
      }
    
    if(check == 0){
      this.badCreds = true;
    }
  }

  getUser(name: string, password: string): void {
    
    this.dataService.login(name, password).subscribe(
    (res: any[]) => {
      this.ids = res;
    },
    (err) => {
      this.error = err;
    }
  );
}

logOut(){
  this.dataService.logout();
  this.loggedUserId = 0;
  this.loggedIn = false;
  this.badCreds = false;
  this.ids = [];
}

ngOnInit(): void {
  
}

}



export class User{
  constructor(
  name: string,
  password: string,
  id?: number
  ){}
}