import { Component, Input, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-year',
  templateUrl: './year.component.html',
  styleUrls: ['./year.component.css']
})
export class YearComponent implements OnInit {
  @ViewChild("outlet", {read: ViewContainerRef}) outletRef: ViewContainerRef;
  @ViewChild("content", {read: TemplateRef}) contentRef: TemplateRef<any>;

  year: number = 0;
  render: boolean = false;
  _loggedUser:number = 1;

  @Input('loggedUserId')
  set loggedUserId(lu:number){
    this._loggedUser = lu;
  }

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(data: any){
    this.year = data.year;
    this.render = true;
    this.outletRef.clear();
    this.outletRef.createEmbeddedView(this.contentRef);
  }

}
