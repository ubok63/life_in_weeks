<?php

class Event{
    public $id;
    public $user_id;
    public $year;
    public $week;
    public $mostCommon;
    public $mostImportant;

    function __construct(){
}

    function get_user_id(){
        return $this->user_id;
    }

    function set_user_id($user_id){
        $this->user_id = $user_id;
    }

    function get_year(){
        return $this->year;
    }

    function set_year($year){
        $this->year = $year;
    }

    function get_week(){
        return $this->week;
    }

    function set_week($week){
        $this->week = $week;
    }

    function get_mostCommon(){
        return $this->mostCommon;
    }

    function set_mostCommon($mostCommon){
        $this->mostCommon = $mostCommon;
    }

    function get_mostImportant(){
        return $this->mostImportant;
    }

    function set_mostImportant($mostImportant){
        $this->mostImportant = $mostImportant;
    }

}