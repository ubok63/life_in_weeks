<?php
class User
{
    public $id;
    public $name;
    public $password;

    function get_name(){
        return $this->name;
    }

    function set_name($name){
        $this->name = $name;
    }

    function get_id(){
        return $this->id;
    }

    function set_id($id){
        $this->id = $id;
    }

    function get_password(){
        return $this->password;
    }

    function set_password($password){
        $this->password = $password;
    }
}