<?php
require 'repositories/SQLUserRepository.php';
require_once './connection.php';
require "../vendor/autoload.php";
use \Firebase\JWT\JWT;

if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('Access-Control-Allow-Headers: X-Requested-With');
    header("HTTP/1.1 200 OK");
    die();
}
$postdata = file_get_contents("php://input");
if(isset($postdata) && !empty($postdata)) {

    $request = json_decode($postdata);

    if ($request === NULL) {
        return http_response_code(400);
    }

    $con = OpenCon();
    //$name = ($_GET['login'] !== null) ? $_GET['login'] : false;
    //$pass = ($_GET['password'] !== null) ? $_GET['password'] : false;

    $name = $request->login;
    $pass = $request->password;

    if (!$name || !$pass) {
        return http_response_code(400);
    }

    $userRepo = new SQLUserRepository($con);

    $user = $userRepo->login($name);

    $id = $user[0]['id'];
    $selectedPass = $user[0]['password'];

    if (!((hash('sha1', $pass) == $selectedPass))) {
        return http_response_code(401);
    } else {
        $secret_key = "SECRET_KEY";
        $token = array(
            "data" => array(
                "id" => $id
            ));

        http_response_code(200);

        $jwt = JWT::encode($token, "SECRET_KEY", 'HS256');
        echo json_encode(["data" => $jwt]);
        //echo json_encode(['data'=>$id]);
    }
}

