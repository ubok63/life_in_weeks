<?php
require 'repositories/SQLEventRepository.php';
require_once './connection.php';
ini_set("allow_url_fopen", true);
require "../vendor/autoload.php";
use \Firebase\JWT\JWT;

if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('Access-Control-Allow-Headers: X-Requested-With');
    header("HTTP/1.1 200 OK");
    die();
}

$con = OpenCon();
$postdata = file_get_contents("php://input");
if(isset($postdata) && !empty($postdata))
{
    $request = json_decode($postdata);

    if($request === NULL){
        return http_response_code(400);
    }

    $authHeader = $_SERVER['HTTP_AUTHORIZATION'];
    $temp_header = explode(" ", $authHeader);
    $jwt = $temp_header[1];

    try {
        $decoded = JWT::decode($jwt,"SECRET_KEY", array('HS256'));

        $user_id = (int)$decoded->data->id;
        $year = trim($request->data->year);
        $week = trim($request->data->week);
        $mostCommon = trim($request->data->mostCommon);
        $mostImportant = trim($request->data->mostImportant);

        $event = new Event();
        $event->set_user_id($decoded->data->id);
        $event->set_mostCommon($mostCommon);
        $event->set_mostImportant($mostImportant);
        $event->set_week($week);
        $event->set_year($year);

        $eventRepo = new SQLEventRepository($con);


        $eventA = [
            'week' => $week,
            'year' => $year,
            'user_id' => $user_id,
            'mostCommon' => $mostCommon,
            'mostImportant' => $mostImportant,
            'id'    => $eventRepo->save($event)
        ];

        echo json_encode(['data'=>$eventA]);



    }catch (Exception $e){
        return http_response_code(401);
    }


}