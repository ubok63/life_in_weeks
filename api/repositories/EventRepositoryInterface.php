<?php
require './classes/Event.php';

interface EventRepositoryInterface
{
    public function getByUserId($id);
    public function remove($id, $idUser);
    public function save(Event $event);
}