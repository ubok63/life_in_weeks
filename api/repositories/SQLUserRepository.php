<?php
require 'UserRepositoryInterface.php';
require_once './connection.php';
class SQLUserRepository implements UserRepositoryInterface
{
    protected $con;

    public function __construct($con)
    {
        $this->con = $con;
    }

    public function login($name)
    {
        $sql = $this->con->prepare("SELECT id, password FROM user where name = :name");
        $sql->bindParam(':name', $name);
        $sql->execute();
        return $sql->fetchAll(PDO::FETCH_ASSOC);
    }

}