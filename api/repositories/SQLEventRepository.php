<?php
require 'EventRepositoryInterface.php';

class SQLEventRepository implements EventRepositoryInterface
{
    protected $con;

    public function __construct($con)
    {
        $this->con = $con;
    }

    public function getByUserId($id)
    {
        $sql = $this->con->prepare("SELECT id, id_user, year, week, mostCommon, mostImportant FROM event where id_user = :id");
        $sql->bindParam(':id', $id);
        $sql->execute();
        return $sql->fetchAll(PDO::FETCH_ASSOC);
    }

    public function save(Event $event)
    {
        $sql = $this->con->prepare("INSERT INTO event(id,id_user,year,week,mostCommon,mostImportant)VALUES (null, :user_id, :year,:week, :mostCommon, :mostImportant)");
        $sql->bindValue(':user_id', $event->get_user_id());
        $sql->bindValue(':year', $event->get_year());
        $sql->bindValue(':week', $event->get_week());
        $sql->bindValue(':mostCommon', $event->get_mostCommon());
        $sql->bindValue(':mostImportant', $event->get_mostImportant());
        if(!$sql->execute()){
            return http_response_code(422);
        }
        return$this->con->lastInsertId();
    }

    public function remove($id, $idUser)
    {
        $sql = $this->con->prepare("DELETE FROM event WHERE id = :id AND id_user = :idUser");
        $sql->bindParam(':id', $id);
        $sql->bindParam(':idUser', $idUser);
        if(!$sql->execute())
        {
            return http_response_code(422);
        }
    }
}