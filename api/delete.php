<?php
require 'repositories/SQLEventRepository.php';
require_once './connection.php';
require "../vendor/autoload.php";
use \Firebase\JWT\JWT;

if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('Access-Control-Allow-Headers: X-Requested-With');
    header("HTTP/1.1 200 OK");
    die();
}

$con = OpenCon();
$id = ($_GET['id'] !== null && (int)$_GET['id'] > 0)? (int)$_GET['id'] : false;

if(!$id)
{
    return http_response_code(400);
}

$authHeader = $_SERVER['HTTP_AUTHORIZATION'];
$temp_header = explode(" ", $authHeader);
$jwt = $temp_header[1];

try {
    $decoded = JWT::decode($jwt,"SECRET_KEY", array('HS256'));

    $idUser = $decoded->data->id;

    echo $idUser;

    $eventRepo = new SQLEventRepository($con);
    $eventRepo->remove($id, $idUser);

}catch (Exception $e){
    return http_response_code(401);
}


