<?php
function OpenCon()
{
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "heslo";
    $db = "life_in_weeks";

    try {
        $conn = new PDO("mysql:host=$dbhost;dbname=$db", $dbuser, $dbpass);

        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }

    return $conn;
}

function CloseCon($conn)
{
    $conn -> close();
}

?>