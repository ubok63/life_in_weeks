<?php
require 'repositories/SQLEventRepository.php';
require_once './connection.php';
require "../vendor/autoload.php";
use \Firebase\JWT\JWT;
if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('Access-Control-Allow-Headers: X-Requested-With');
    header("HTTP/1.1 200 OK");
    die();
}
/*$id = ($_GET['id'] !== null && (int)$_GET['id'] > 0)? (int)$_GET['id'] : false;
if(!$id)
{
    return http_response_code(400);
}*/

$authHeader = $_SERVER['HTTP_AUTHORIZATION'];
$temp_header = explode(" ", $authHeader);
$jwt = $temp_header[1];

$con = OpenCon();

try {
    $decoded = JWT::decode($jwt, "SECRET_KEY", ['HS256']);

    $id = $decoded->data->id;

    $eventRepo = new SQLEventRepository($con);
    echo json_encode(['data'=>$eventRepo->getByUserId($id)]);

}catch (Exception $e){
    return http_response_code(401);
}



